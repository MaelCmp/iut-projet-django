# Projet Django : iut-projet-django

## Auteurs : ADZUAR Rémy, PICART Quentin, CAMPO Maël

# Versionnage

## Convention de nommage

cf( https://www.grafikart.fr/tutoriels/nommage-commit-1009 )

# Installation

- créer son environnement virtuel : `virtualenv -p python3 venv`
- activer l'environnement virtuel : `source venv/bin/activate`
- migrer la base de données : `./manage.py migrate`
- installer les dependances du projet (bootstrap notamment) :
    - `cd static ` (dans le root)
    - `npm i`

## Lancer le serveur

Commande pour lancer le serveur :
`./manage.py runserver`


## Accès à l'application principale
Vous pouvez accéder à l'application en cliquant sur [ce lien](http://127.0.0.1:8000/series/)


## Routes disponibles

```bash

    admin/


    series/
    series/home

```